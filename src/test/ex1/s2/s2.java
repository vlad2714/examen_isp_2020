package s2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

public class s2 extends JFrame {
    JButton button;
    JTextField textField;
    JTextArea textArea;
    s2()
    {
        setTitle("s2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        int width = 200;
        int height= 30;

        button = new JButton("Press to open");
        button.setBounds(100, 100,width,height);
        button.addActionListener(new PressButton());

        textField=new JTextField(15);
        textField.setBounds(100,60,width,height);

        textArea = new JTextArea();
        textArea.setBounds(100,160,width, 4* height);

        add(button);
        add(textField);
        add(textArea);

        setSize(400,400);
        setVisible(true);
    }
    void open(String f) {
        try {
            textArea.setText("");
            BufferedReader bf =
                    new BufferedReader(
                            new FileReader("D:\\\\Projects\\\\examen_isp_2020\\\\src\\\\test\\\\ex1\\\\test.txt" + f));
            String l;
            l = bf.readLine();
            while (l != null) {
                textArea.append(l + "\n");
                l = bf.readLine();
            }
        } catch (Exception e) {
            System.out.println("File not found!");
        }
    }
    class PressButton implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String namefile = textArea.getText();
            open(namefile);
        }
    }
    public static void main(String[] args){
        new s2();
    }
}